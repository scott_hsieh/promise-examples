var api_endpoint = 'https://test.one45.com/MOBILE_INTEGRATION/web/one45_stage.php'
var api_path = '/public/api/v1/institution';

function getInstitution() {
  return {
  "id": "125",
  "name": "Loma Linda University",
  "abbreviation": null,
  "base_url": "llu.one45.com",
  "webeval_url": "https://llu.one45.com/index.php",
  "logo": {
    "name": "school_logo.png",
    "url": "https://test.one45.com/MOBILE_INTEGRATION/admin/files/logos/school_logo.png",
    "width": "94",
    "height": "32"
  },
  "primary_color": "#cc0000",
  "secondary_color": "#f2f2f2"
  }
}

function getInstitutionAsync() {
  $.ajax({
  method: 'GET',
  url: api_endpoint + api_path,
  data: null,
  success: function(data){
    return data
  }
});
}

