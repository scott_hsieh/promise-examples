var api_endpoint = 'https://test.one45.com/MOBILE_INTEGRATION/web/one45_stage.php'
var client_key = '1d8rczly7xogwk0w80cgsogw40k0ocsww0cc484k08wsksow4c';
var generate_token_path = '/public/api/v1/token/login';
var user_todos_path = '/api/v1/user/todos';


function generateUserToken(username, password) {
  return $.ajax({
  method: 'GET',
  url: api_endpoint + generate_token_path,
  data:{'username': username, 'password': password, 'client_key':client_key },
});
}


function getUserTodo(response) {
  return $.ajax({
  method: 'GET',
  url: api_endpoint + user_todos_path,
  data:{'access_token': response.access_token}
});
}

function displayUserTodos(todos) {
  $("#todo-count").text(todos.length + ' Todos');
}
