(function e(t,n,r){function s(o,u){if(!n[o]){if(!t[o]){var a=typeof require=="function"&&require;if(!u&&a)return a(o,!0);if(i)return i(o,!0);var f=new Error("Cannot find module '"+o+"'");throw f.code="MODULE_NOT_FOUND",f}var l=n[o]={exports:{}};t[o][0].call(l.exports,function(e){var n=t[o][1][e];return s(n?n:e)},l,l.exports,e,t,n,r)}return n[o].exports}var i=typeof require=="function"&&require;for(var o=0;o<r.length;o++)s(r[o]);return s})({1:[function(require,module,exports){
"use strict";

Object.defineProperty(exports, '__esModule', {
  value: true
});

var _createClass = (function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ('value' in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; })();

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { 'default': obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError('Cannot call a class as a function'); } }

var _core = require('./core');

var _core2 = _interopRequireDefault(_core);

var _rest = require('./rest');

var _rest2 = _interopRequireDefault(_rest);

var _auth = require('./auth');

var _auth2 = _interopRequireDefault(_auth);

var _One45User = require('./One45User');

var _One45User2 = _interopRequireDefault(_One45User);

var _One45Form = require('./One45Form');

var _One45Form2 = _interopRequireDefault(_One45Form);

var _One45Institution = require('./One45Institution');

var _One45Institution2 = _interopRequireDefault(_One45Institution);

/**
 * Wrapper class for all of one45 services and configuration
 */

var One45 = (function () {
  function One45() {
    _classCallCheck(this, One45);

    this.Core = _core2['default'];
    this.Rest = _rest2['default'];
    this.Auth = _auth2['default'];
    this.User = _One45User2['default'];
    this.Form = _One45Form2['default'];
    this.Institution = _One45Institution2['default'];
  }

  /**
   * @param serverUrl - base url end point for making api requests
   * @param clientKey - application key
   * @param config - key value array of parameters that can be set ie. CURRENT_USER, REFRESH_TOKEN, ACCESS_TOKEN
   */

  _createClass(One45, [{
    key: 'initialize',
    value: function initialize() {
      var serverUrl = arguments.length <= 0 || arguments[0] === undefined ? '' : arguments[0];

      var _this = this;

      var clientKey = arguments.length <= 1 || arguments[1] === undefined ? '' : arguments[1];
      var configs = arguments.length <= 2 || arguments[2] === undefined ? null : arguments[2];

      this.Core.set('SERVER_URL', serverUrl);
      this.Core.set('CLIENT_KEY', clientKey);

      if (configs instanceof Array) {
        configs.foreach(function (value, key) {
          return _this.Core.set(key, value);
        });
      }

      return this.Institution.getCurrentInstitution();
    }
  }]);

  return One45;
})();

exports['default'] = One45;
module.exports = exports['default'];

},{"./One45Form":2,"./One45Institution":3,"./One45User":4,"./auth":5,"./core":7,"./rest":9}],2:[function(require,module,exports){
'use strict';

Object.defineProperty(exports, '__esModule', {
  value: true
});

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { 'default': obj }; }

var _core = require('./core');

var _core2 = _interopRequireDefault(_core);

var _rest = require('./rest');

var _rest2 = _interopRequireDefault(_rest);

function getById(form_id) {
  return _rest2['default'].get('/api/v1/forms/' + form_id);
}

function getQuestions(form_id) {
  return _rest2['default'].get('/api/v1/forms/' + form_id + '/questions');
}

exports['default'] = {
  getById: getById,
  getQuestions: getQuestions
};
module.exports = exports['default'];

},{"./core":7,"./rest":9}],3:[function(require,module,exports){
'use strict';

Object.defineProperty(exports, '__esModule', {
  value: true
});

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { 'default': obj }; }

var _core = require('./core');

var _core2 = _interopRequireDefault(_core);

var _rest = require('./rest');

var _rest2 = _interopRequireDefault(_rest);

function getCurrentInstitution() {
  return _rest2['default'].get('/public/api/v1/institution');
}

function getPreferences(params) {
  return _rest2['default'].get('/api/v1/institution/preferences', params);
}

exports['default'] = {
  getCurrentInstitution: getCurrentInstitution,
  getPreferences: getPreferences
};
module.exports = exports['default'];

},{"./core":7,"./rest":9}],4:[function(require,module,exports){
'use strict';

Object.defineProperty(exports, '__esModule', {
  value: true
});

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { 'default': obj }; }

var _core = require('./core');

var _core2 = _interopRequireDefault(_core);

var _rest = require('./rest');

var _rest2 = _interopRequireDefault(_rest);

function login(username, password) {
  return _rest2['default'].get('/public/api/v1/token/login', {
    username: username,
    password: password,
    client_key: _core2['default'].get('CLIENT_KEY')
  }).then(function (res) {
    _core2['default'].set('ACCESS_TOKEN', res.access_token);
    _core2['default'].set('REFRESH_TOKEN', res.refresh_token);
    _core2['default'].set('CURRENT_USER', res.user);
    return res;
  });
}

/**
 * Todo : Should return promise and invalidate token using invalidate api
 */
function logout() {
  _core2['default'].set('ACCESS_TOKEN', null);
  _core2['default'].set('REFRESH_TOKEN', null);
  _core2['default'].set('CURRENT_USER', null);
}

function getCurrentUser() {
  return _core2['default'].get('CURRENT_USER');
}

function getTodos(params) {
  return _rest2['default'].get('/api/v1/user/todos', params);
}

function getEvaluationById(id) {
  return _rest2['default'].get('/api/v1/user/evaluations/' + id);
}

function saveAnswer(evaluation_id, question_id, values) {
  var url = '/api/v1/user/evaluations/' + evaluation_id + '/question/' + question_id + '/answer';
  return _rest2['default'].put(url, values);
}

function patchEvaluation(evaluation_id, values) {
  var url = '/api/v1/user/evaluations/' + evaluation_id;
  return _rest2['default'].patch(url, values);
}

exports['default'] = {
  login: login,
  logout: logout,
  getCurrentUser: getCurrentUser,
  getTodos: getTodos,
  getEvaluationById: getEvaluationById,
  saveAnswer: saveAnswer,
  patchEvaluation: patchEvaluation
};
module.exports = exports['default'];

},{"./core":7,"./rest":9}],5:[function(require,module,exports){
'use strict';

Object.defineProperty(exports, '__esModule', {
  value: true
});

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { 'default': obj }; }

var _core = require('./core');

var _core2 = _interopRequireDefault(_core);

var _rest = require('./rest');

var _rest2 = _interopRequireDefault(_rest);

var _config = require('./config');

var _config2 = _interopRequireDefault(_config);

// try to get token using refresh token; if failed,
// fallback and try to get token by client credentials
function regenerateToken() {
  return generateTokenByRefreshToken()['catch'](function (err) {
    return generateTokenByClientCredentials();
  });
}

function generateTokenByRefreshToken() {
  var refreshToken = _core2['default'].get('REFRESH_TOKEN');

  // immediately reject without attempting request if refresh token
  // is not set
  if (!refreshToken) {
    return Promise.reject(new Error('Refresh token is not set'));
  }

  return _rest2['default'].get('/public/api/v1/token/refresh', {
    refresh_token: refreshToken
  }).then(function (res) {
    _core2['default'].set('ACCESS_TOKEN', res.access_token);
    _core2['default'].set('REFRESH_TOKEN', res.refresh_token);
    return res;
  });
}

function generateTokenByClientCredentials() {
  var clientKey = _core2['default'].get('CLIENT_KEY'),
      clientSecret = _core2['default'].get('CLIENT_SECRET');

  // immediately reject without attempting request if client key or secret
  // is not set
  if (!clientKey || !clientSecret) {
    return Promise.reject(new Error('Client key or secret is not set'));
  }

  return _rest2['default'].get('/public/api/v1/token/generate', {
    client_key: clientKey,
    client_secret: clientSecret
  }).then(function (res) {
    _core2['default'].set('ACCESS_TOKEN', res.access_token);
    _core2['default'].set('REFRESH_TOKEN', res.refresh_token);
    return res;
  });
}

exports['default'] = {
  regenerateToken: regenerateToken,
  generateTokenByRefreshToken: generateTokenByRefreshToken,
  generateTokenByClientCredentials: generateTokenByClientCredentials
};
module.exports = exports['default'];

},{"./config":6,"./core":7,"./rest":9}],6:[function(require,module,exports){
'use strict';

Object.defineProperty(exports, '__esModule', {
  value: true
});
exports['default'] = {
  tokenPath: '/public/api/v1/token/refresh'
};
module.exports = exports['default'];

},{}],7:[function(require,module,exports){
'use strict';

/**
 * Core class to manage state such as auth credentials and current user
 */
Object.defineProperty(exports, '__esModule', {
  value: true
});

var _createClass = (function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ('value' in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; })();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError('Cannot call a class as a function'); } }

var CoreManager = (function () {
  function CoreManager() {
    var _this = this;

    _classCallCheck(this, CoreManager);

    var keys = ['SERVER_URL', 'CLIENT_KEY', 'CLIENT_SECRET', 'ACCESS_TOKEN', 'REFRESH_TOKEN', 'CURRENT_USER'];

    this.keys = keys;
    keys.forEach(function (key) {
      return _this[key] = null;
    });
  }

  _createClass(CoreManager, [{
    key: 'get',
    value: function get(key) {
      return this.isValid(key) ? this[key] : null;
    }
  }, {
    key: 'set',
    value: function set(key, value) {
      this.isValid(key) && (this[key] = value);
    }
  }, {
    key: 'isValid',
    value: function isValid(key) {
      return this.keys.indexOf(key) >= 0;
    }
  }]);

  return CoreManager;
})();

var core = undefined;

exports['default'] = (function getCore() {
  core = core || new CoreManager();
  return core;
})();

module.exports = exports['default'];

},{}],8:[function(require,module,exports){
"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

var _One45 = require('./One45');

var _One452 = _interopRequireDefault(_One45);

exports["default"] = _One452["default"];

window && (window.One45 = _One452["default"]);
module.exports = exports["default"];

},{"./One45":1}],9:[function(require,module,exports){
'use strict';

Object.defineProperty(exports, '__esModule', {
  value: true
});

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { 'default': obj }; }

var _core = require('./core');

var _core2 = _interopRequireDefault(_core);

var _auth = require('./auth');

var _auth2 = _interopRequireDefault(_auth);

var methods = {
  GET: 'GET', POST: 'POST', PUT: 'PUT', PATCH: 'PATCH', DELETE: 'DELETE',
  OPTIONS: 'OPTIONS', LINK: 'LINK', HEAD: 'HEAD'
};

var withPayload = [methods.POST, methods.PUT, methods.PATCH];

// TODO: add custom headers support
function request(url, method) {
  var opts = arguments.length <= 2 || arguments[2] === undefined ? {} : arguments[2];

  // add query params to URL if there are any
  url = _core2['default'].get('SERVER_URL') + url + (opts.params instanceof Object ? _serializeParams(opts.params) : '');

  method = method.toUpperCase();
  if (!methods[method]) {
    return console.error('Unsupported http method: ' + method);
  }

  var makePromise = function makePromise() {
    return new Promise(function (resolve, reject) {
      var req = new XMLHttpRequest(),
          token = _core2['default'].get('ACCESS_TOKEN');

      req.open(method, url, true);

      // add access token to params if one is set
      if (token) {
        req.setRequestHeader('Authorization', 'Bearer ' + token);
      }

      // resolve with response if status code indicates success,
      // otherwise reject with error
      // TODO: handle non-JSON response bodies
      req.onload = function () {
        if (req.status >= 200 && req.status < 300) {
          if (req.response.length > 0) {
            resolve(JSON.parse(req.response));
          } else {
            resolve(null);
          }
        } else {
          reject(_createHttpError(req));
        }
      };

      // reject with error on network errors
      req.onerror = function () {
        return reject(new Error('Network Error'));
      };

      // send the request, including data if method is PUT, PATCH, or POST
      if (withPayload.indexOf(method) >= 0 && opts.data instanceof Object) {
        req.setRequestHeader("Content-Type", "application/json");
        req.send(JSON.stringify(opts.data));
      } else {
        req.send();
      }
    });
  };

  // catch any 401 errors, try to regenerate token and retry (once max)
  return makePromise().then(function (response) {
    return response;
  }, function (err) {
    return err.status != 401 ? Promise.reject(err) : _auth2['default'].regenerateToken().then(function () {
      return makePromise();
    });
  });
}

request.get = function (url) {
  var params = arguments.length <= 1 || arguments[1] === undefined ? {} : arguments[1];
  return request(url, methods.GET, { params: params });
};
request.del = function (url) {
  var params = arguments.length <= 1 || arguments[1] === undefined ? {} : arguments[1];
  return request(url, methods.DELETE, { params: params });
};
request.post = function (url) {
  var data = arguments.length <= 1 || arguments[1] === undefined ? {} : arguments[1];
  return request(url, methods.POST, { data: data });
};
request.patch = function (url) {
  var data = arguments.length <= 1 || arguments[1] === undefined ? {} : arguments[1];
  return request(url, methods.PATCH, { data: data });
};
request.put = function (url) {
  var data = arguments.length <= 1 || arguments[1] === undefined ? {} : arguments[1];
  return request(url, methods.PUT, { data: data });
};

function _serializeParams(params) {
  var str = [];

  for (var prop in params) {
    if (params.hasOwnProperty(prop)) {
      var key = prop,
          value = params[prop];
      // TODO: skip empty vals and trim strings here

      if (Array.isArray(value)) {
        str = str.concat(_serializeArray(key, value));
      } else {
        str.push(encodeURIComponent(key) + "=" + encodeURIComponent(value));
      }
    }
  }

  return str.length > 0 ? '?' + str.join("&") : '';
}

// export serialization fn for testing
request._serializeParams = _serializeParams;

function _serializeArray(key, arr) {
  return arr.map(function (val) {
    return encodeURIComponent(key) + '[]=' + encodeURIComponent(val);
  });
}

function _createHttpError(req) {
  var err = new Error(req.statusText);
  err.status = req.status;
  err.response = JSON.parse(req.response);
  return err;
}

exports['default'] = request;
module.exports = exports['default'];

},{"./auth":5,"./core":7}]},{},[8])
//# sourceMappingURL=data:application/json;charset:utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIm5vZGVfbW9kdWxlcy9icm93c2VyaWZ5L25vZGVfbW9kdWxlcy9icm93c2VyLXBhY2svX3ByZWx1ZGUuanMiLCIvVXNlcnMvc2NvdHRoc2llaC93b3Jrc3BhY2Uvb25lNDUtanMtc2RrL3NyYy9PbmU0NS5qcyIsIi9Vc2Vycy9zY290dGhzaWVoL3dvcmtzcGFjZS9vbmU0NS1qcy1zZGsvc3JjL09uZTQ1Rm9ybS5qcyIsIi9Vc2Vycy9zY290dGhzaWVoL3dvcmtzcGFjZS9vbmU0NS1qcy1zZGsvc3JjL09uZTQ1SW5zdGl0dXRpb24uanMiLCIvVXNlcnMvc2NvdHRoc2llaC93b3Jrc3BhY2Uvb25lNDUtanMtc2RrL3NyYy9PbmU0NVVzZXIuanMiLCIvVXNlcnMvc2NvdHRoc2llaC93b3Jrc3BhY2Uvb25lNDUtanMtc2RrL3NyYy9hdXRoLmpzIiwiL1VzZXJzL3Njb3R0aHNpZWgvd29ya3NwYWNlL29uZTQ1LWpzLXNkay9zcmMvY29uZmlnLmpzIiwiL1VzZXJzL3Njb3R0aHNpZWgvd29ya3NwYWNlL29uZTQ1LWpzLXNkay9zcmMvY29yZS5qcyIsIi9Vc2Vycy9zY290dGhzaWVoL3dvcmtzcGFjZS9vbmU0NS1qcy1zZGsvc3JjL2luZGV4LmpzIiwiL1VzZXJzL3Njb3R0aHNpZWgvd29ya3NwYWNlL29uZTQ1LWpzLXNkay9zcmMvcmVzdC5qcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtBQ0FBLFlBQVksQ0FBQzs7Ozs7Ozs7Ozs7O29CQUVJLFFBQVE7Ozs7b0JBQ1IsUUFBUTs7OztvQkFDUixRQUFROzs7O3lCQUNSLGFBQWE7Ozs7eUJBQ2IsYUFBYTs7OztnQ0FDTixvQkFBb0I7Ozs7Ozs7O0lBS3ZCLEtBQUs7QUFFYixXQUZRLEtBQUssR0FFVjswQkFGSyxLQUFLOztBQUd0QixRQUFJLENBQUMsSUFBSSxvQkFBTyxDQUFDO0FBQ2pCLFFBQUksQ0FBQyxJQUFJLG9CQUFPLENBQUM7QUFDakIsUUFBSSxDQUFDLElBQUksb0JBQU8sQ0FBQztBQUNqQixRQUFJLENBQUMsSUFBSSx5QkFBTyxDQUFDO0FBQ2pCLFFBQUksQ0FBQyxJQUFJLHlCQUFPLENBQUM7QUFDakIsUUFBSSxDQUFDLFdBQVcsZ0NBQWMsQ0FBQztHQUNoQzs7Ozs7Ozs7ZUFUa0IsS0FBSzs7V0FnQmQsc0JBQWlEO1VBQWhELFNBQVMseURBQUcsRUFBRTs7OztVQUFFLFNBQVMseURBQUcsRUFBRTtVQUFFLE9BQU8seURBQUcsSUFBSTs7QUFDdkQsVUFBSSxDQUFDLElBQUksQ0FBQyxHQUFHLENBQUMsWUFBWSxFQUFFLFNBQVMsQ0FBQyxDQUFDO0FBQ3ZDLFVBQUksQ0FBQyxJQUFJLENBQUMsR0FBRyxDQUFDLFlBQVksRUFBRSxTQUFTLENBQUMsQ0FBQzs7QUFFdkMsVUFBSSxPQUFPLFlBQVksS0FBSyxFQUFFO0FBQzVCLGVBQU8sQ0FBQyxPQUFPLENBQUMsVUFBQyxLQUFLLEVBQUUsR0FBRztpQkFBSyxNQUFLLElBQUksQ0FBQyxHQUFHLENBQUMsR0FBRyxFQUFFLEtBQUssQ0FBQztTQUFBLENBQUMsQ0FBQTtPQUMzRDs7QUFFRCxhQUFPLElBQUksQ0FBQyxXQUFXLENBQUMscUJBQXFCLEVBQUUsQ0FBQztLQUNqRDs7O1NBekJrQixLQUFLOzs7cUJBQUwsS0FBSzs7OztBQ1oxQixZQUFZLENBQUM7Ozs7Ozs7O29CQUVJLFFBQVE7Ozs7b0JBQ1IsUUFBUTs7OztBQUV6QixTQUFTLE9BQU8sQ0FBQyxPQUFPLEVBQUU7QUFDeEIsU0FBTyxrQkFBSyxHQUFHLENBQUMsZ0JBQWdCLEdBQUcsT0FBTyxDQUFDLENBQUM7Q0FDN0M7O0FBRUQsU0FBUyxZQUFZLENBQUMsT0FBTyxFQUFFO0FBQzVCLFNBQU8sa0JBQUssR0FBRyxDQUFDLGdCQUFnQixHQUFHLE9BQU8sR0FBRyxZQUFZLENBQUMsQ0FBQztDQUM3RDs7cUJBR2M7QUFDYixTQUFPLEVBQVAsT0FBTztBQUNQLGNBQVksRUFBWixZQUFZO0NBQ2I7Ozs7QUNqQkQsWUFBWSxDQUFDOzs7Ozs7OztvQkFFSSxRQUFROzs7O29CQUNSLFFBQVE7Ozs7QUFFekIsU0FBUyxxQkFBcUIsR0FBRztBQUMvQixTQUFPLGtCQUFLLEdBQUcsQ0FBQyw0QkFBNEIsQ0FBQyxDQUFDO0NBQy9DOztBQUVELFNBQVMsY0FBYyxDQUFDLE1BQU0sRUFBRTtBQUM5QixTQUFPLGtCQUFLLEdBQUcsQ0FBQyxpQ0FBaUMsRUFBRSxNQUFNLENBQUMsQ0FBQztDQUM1RDs7cUJBR2M7QUFDYix1QkFBcUIsRUFBckIscUJBQXFCO0FBQ3JCLGdCQUFjLEVBQWQsY0FBYztDQUNmOzs7O0FDakJELFlBQVksQ0FBQzs7Ozs7Ozs7b0JBRUksUUFBUTs7OztvQkFDUixRQUFROzs7O0FBRXpCLFNBQVMsS0FBSyxDQUFDLFFBQVEsRUFBRSxRQUFRLEVBQUU7QUFDakMsU0FBTyxrQkFBSyxHQUFHLENBQUMsNEJBQTRCLEVBQUU7QUFDNUMsWUFBUSxFQUFSLFFBQVE7QUFDUixZQUFRLEVBQVIsUUFBUTtBQUNSLGNBQVUsRUFBRSxrQkFBSyxHQUFHLENBQUMsWUFBWSxDQUFDO0dBQ25DLENBQUMsQ0FBQyxJQUFJLENBQUMsVUFBQSxHQUFHLEVBQUk7QUFDYixzQkFBSyxHQUFHLENBQUMsY0FBYyxFQUFFLEdBQUcsQ0FBQyxZQUFZLENBQUMsQ0FBQztBQUMzQyxzQkFBSyxHQUFHLENBQUMsZUFBZSxFQUFFLEdBQUcsQ0FBQyxhQUFhLENBQUMsQ0FBQztBQUM3QyxzQkFBSyxHQUFHLENBQUMsY0FBYyxFQUFFLEdBQUcsQ0FBQyxJQUFJLENBQUMsQ0FBQztBQUNuQyxXQUFPLEdBQUcsQ0FBQztHQUNaLENBQUMsQ0FBQztDQUNKOzs7OztBQUtELFNBQVMsTUFBTSxHQUFHO0FBQ2Qsb0JBQUssR0FBRyxDQUFDLGNBQWMsRUFBRSxJQUFJLENBQUMsQ0FBQztBQUMvQixvQkFBSyxHQUFHLENBQUMsZUFBZSxFQUFFLElBQUksQ0FBQyxDQUFDO0FBQ2hDLG9CQUFLLEdBQUcsQ0FBQyxjQUFjLEVBQUUsSUFBSSxDQUFDLENBQUM7Q0FDbEM7O0FBRUQsU0FBUyxjQUFjLEdBQUc7QUFDeEIsU0FBTyxrQkFBSyxHQUFHLENBQUMsY0FBYyxDQUFDLENBQUM7Q0FDakM7O0FBRUQsU0FBUyxRQUFRLENBQUMsTUFBTSxFQUFFO0FBQ3hCLFNBQU8sa0JBQUssR0FBRyxDQUFDLG9CQUFvQixFQUFFLE1BQU0sQ0FBQyxDQUFDO0NBQy9DOztBQUVELFNBQVMsaUJBQWlCLENBQUMsRUFBRSxFQUFFO0FBQzdCLFNBQU8sa0JBQUssR0FBRyxDQUFDLDJCQUEyQixHQUFHLEVBQUUsQ0FBQyxDQUFDO0NBQ25EOztBQUVELFNBQVMsVUFBVSxDQUFDLGFBQWEsRUFBRSxXQUFXLEVBQUUsTUFBTSxFQUFFO0FBQ3RELE1BQUksR0FBRyxHQUFHLDJCQUEyQixHQUFHLGFBQWEsR0FBRyxZQUFZLEdBQUcsV0FBVyxHQUFHLFNBQVMsQ0FBQztBQUMvRixTQUFPLGtCQUFLLEdBQUcsQ0FBQyxHQUFHLEVBQUUsTUFBTSxDQUFDLENBQUM7Q0FDOUI7O0FBRUQsU0FBUyxlQUFlLENBQUMsYUFBYSxFQUFFLE1BQU0sRUFBRTtBQUM5QyxNQUFJLEdBQUcsR0FBRywyQkFBMkIsR0FBRyxhQUFhLENBQUM7QUFDdEQsU0FBTyxrQkFBSyxLQUFLLENBQUMsR0FBRyxFQUFFLE1BQU0sQ0FBQyxDQUFDO0NBQ2hDOztxQkFFYztBQUNiLE9BQUssRUFBTCxLQUFLO0FBQ0wsUUFBTSxFQUFOLE1BQU07QUFDTixnQkFBYyxFQUFkLGNBQWM7QUFDZCxVQUFRLEVBQVIsUUFBUTtBQUNSLG1CQUFpQixFQUFqQixpQkFBaUI7QUFDakIsWUFBVSxFQUFWLFVBQVU7QUFDVixpQkFBZSxFQUFmLGVBQWU7Q0FDaEI7Ozs7QUN6REQsWUFBWSxDQUFDOzs7Ozs7OztvQkFFSSxRQUFROzs7O29CQUNSLFFBQVE7Ozs7c0JBQ04sVUFBVTs7Ozs7O0FBSTdCLFNBQVMsZUFBZSxHQUFHO0FBQ3pCLFNBQU8sMkJBQTJCLEVBQUUsU0FBTSxDQUFDLFVBQUEsR0FBRztXQUM1QyxnQ0FBZ0MsRUFBRTtHQUFBLENBQ25DLENBQUM7Q0FDSDs7QUFFRCxTQUFTLDJCQUEyQixHQUFHO0FBQ3JDLE1BQUksWUFBWSxHQUFHLGtCQUFLLEdBQUcsQ0FBQyxlQUFlLENBQUMsQ0FBQzs7OztBQUk3QyxNQUFJLENBQUMsWUFBWSxFQUFFO0FBQ2pCLFdBQU8sT0FBTyxDQUFDLE1BQU0sQ0FBQyxJQUFJLEtBQUssQ0FBQywwQkFBMEIsQ0FBQyxDQUFDLENBQUM7R0FDOUQ7O0FBRUQsU0FBTyxrQkFBSyxHQUFHLENBQUMsOEJBQThCLEVBQUU7QUFDOUMsaUJBQWEsRUFBRSxZQUFZO0dBQzVCLENBQUMsQ0FBQyxJQUFJLENBQUMsVUFBQSxHQUFHLEVBQUk7QUFDYixzQkFBSyxHQUFHLENBQUMsY0FBYyxFQUFFLEdBQUcsQ0FBQyxZQUFZLENBQUMsQ0FBQztBQUMzQyxzQkFBSyxHQUFHLENBQUMsZUFBZSxFQUFFLEdBQUcsQ0FBQyxhQUFhLENBQUMsQ0FBQztBQUM3QyxXQUFPLEdBQUcsQ0FBQztHQUNaLENBQUMsQ0FBQztDQUNKOztBQUVELFNBQVMsZ0NBQWdDLEdBQUc7QUFDMUMsTUFBSSxTQUFTLEdBQUcsa0JBQUssR0FBRyxDQUFDLFlBQVksQ0FBQztNQUNsQyxZQUFZLEdBQUcsa0JBQUssR0FBRyxDQUFDLGVBQWUsQ0FBQyxDQUFDOzs7O0FBSTdDLE1BQUksQ0FBQyxTQUFTLElBQUksQ0FBQyxZQUFZLEVBQUU7QUFDL0IsV0FBTyxPQUFPLENBQUMsTUFBTSxDQUFDLElBQUksS0FBSyxDQUFDLGlDQUFpQyxDQUFDLENBQUMsQ0FBQztHQUNyRTs7QUFFRCxTQUFPLGtCQUFLLEdBQUcsQ0FBQywrQkFBK0IsRUFBRTtBQUMvQyxjQUFVLEVBQUUsU0FBUztBQUNyQixpQkFBYSxFQUFFLFlBQVk7R0FDNUIsQ0FBQyxDQUFDLElBQUksQ0FBQyxVQUFBLEdBQUcsRUFBSTtBQUNiLHNCQUFLLEdBQUcsQ0FBQyxjQUFjLEVBQUUsR0FBRyxDQUFDLFlBQVksQ0FBQyxDQUFDO0FBQzNDLHNCQUFLLEdBQUcsQ0FBQyxlQUFlLEVBQUUsR0FBRyxDQUFDLGFBQWEsQ0FBQyxDQUFDO0FBQzdDLFdBQU8sR0FBRyxDQUFDO0dBQ1osQ0FBQyxDQUFDO0NBQ0o7O3FCQUVjO0FBQ2IsaUJBQWUsRUFBZixlQUFlO0FBQ2YsNkJBQTJCLEVBQTNCLDJCQUEyQjtBQUMzQixrQ0FBZ0MsRUFBaEMsZ0NBQWdDO0NBQ2pDOzs7O0FDeERELFlBQVksQ0FBQzs7Ozs7cUJBRUU7QUFDYixXQUFTLEVBQUUsOEJBQThCO0NBQzFDOzs7O0FDSkQsWUFBWSxDQUFDOzs7Ozs7Ozs7Ozs7O0lBS1AsV0FBVztBQUVKLFdBRlAsV0FBVyxHQUVEOzs7MEJBRlYsV0FBVzs7QUFHYixRQUFJLElBQUksR0FBRyxDQUNULFlBQVksRUFBRSxZQUFZLEVBQUUsZUFBZSxFQUMzQyxjQUFjLEVBQUUsZUFBZSxFQUFFLGNBQWMsQ0FDaEQsQ0FBQzs7QUFFRixRQUFJLENBQUMsSUFBSSxHQUFHLElBQUksQ0FBQztBQUNqQixRQUFJLENBQUMsT0FBTyxDQUFDLFVBQUEsR0FBRzthQUFJLE1BQUssR0FBRyxDQUFDLEdBQUcsSUFBSTtLQUFBLENBQUMsQ0FBQztHQUN2Qzs7ZUFWRyxXQUFXOztXQVlaLGFBQUMsR0FBRyxFQUFFO0FBQ1AsYUFBTyxJQUFJLENBQUMsT0FBTyxDQUFDLEdBQUcsQ0FBQyxHQUFHLElBQUksQ0FBQyxHQUFHLENBQUMsR0FBRyxJQUFJLENBQUM7S0FDN0M7OztXQUVFLGFBQUMsR0FBRyxFQUFFLEtBQUssRUFBRTtBQUNkLFVBQUksQ0FBQyxPQUFPLENBQUMsR0FBRyxDQUFDLEtBQUssSUFBSSxDQUFDLEdBQUcsQ0FBQyxHQUFHLEtBQUssQ0FBQSxBQUFDLENBQUM7S0FDMUM7OztXQUVNLGlCQUFDLEdBQUcsRUFBRTtBQUNYLGFBQU8sSUFBSSxDQUFDLElBQUksQ0FBQyxPQUFPLENBQUMsR0FBRyxDQUFDLElBQUksQ0FBQyxDQUFDO0tBQ3BDOzs7U0F0QkcsV0FBVzs7O0FBMEJqQixJQUFJLElBQUksWUFBQSxDQUFDOztxQkFFTSxDQUFDLFNBQVMsT0FBTyxHQUFHO0FBQ2pDLE1BQUksR0FBRyxJQUFJLElBQUksSUFBSSxXQUFXLEVBQUUsQ0FBQztBQUNqQyxTQUFPLElBQUksQ0FBQztDQUNiLENBQUEsRUFBRzs7Ozs7QUNwQ0osWUFBWSxDQUFDOzs7Ozs7OztxQkFFSyxTQUFTOzs7Ozs7QUFHM0IsTUFBTSxLQUFLLE1BQU0sQ0FBQyxLQUFLLHNCQUFRLEFBQUMsQ0FBQzs7OztBQ0xqQyxZQUFZLENBQUM7Ozs7Ozs7O29CQUVJLFFBQVE7Ozs7b0JBQ1IsUUFBUTs7OztBQUV6QixJQUFNLE9BQU8sR0FBRztBQUNkLEtBQUcsRUFBRSxLQUFLLEVBQUUsSUFBSSxFQUFFLE1BQU0sRUFBRSxHQUFHLEVBQUUsS0FBSyxFQUFFLEtBQUssRUFBRSxPQUFPLEVBQUUsTUFBTSxFQUFFLFFBQVE7QUFDdEUsU0FBTyxFQUFFLFNBQVMsRUFBRSxJQUFJLEVBQUUsTUFBTSxFQUFFLElBQUksRUFBRSxNQUFNO0NBQy9DLENBQUM7O0FBRUYsSUFBTSxXQUFXLEdBQUcsQ0FBRSxPQUFPLENBQUMsSUFBSSxFQUFFLE9BQU8sQ0FBQyxHQUFHLEVBQUUsT0FBTyxDQUFDLEtBQUssQ0FBRSxDQUFDOzs7QUFHakUsU0FBUyxPQUFPLENBQUMsR0FBRyxFQUFFLE1BQU0sRUFBVztNQUFULElBQUkseURBQUMsRUFBRTs7O0FBRW5DLEtBQUcsR0FBRyxrQkFBSyxHQUFHLENBQUMsWUFBWSxDQUFDLEdBQUcsR0FBRyxJQUM3QixBQUFDLElBQUksQ0FBQyxNQUFNLFlBQVksTUFBTSxHQUFJLGdCQUFnQixDQUFDLElBQUksQ0FBQyxNQUFNLENBQUMsR0FBRyxFQUFFLENBQUEsQUFBQyxDQUFDOztBQUUzRSxRQUFNLEdBQUcsTUFBTSxDQUFDLFdBQVcsRUFBRSxDQUFDO0FBQzlCLE1BQUksQ0FBQyxPQUFPLENBQUMsTUFBTSxDQUFDLEVBQUU7QUFDcEIsV0FBTyxPQUFPLENBQUMsS0FBSyxDQUFDLDJCQUEyQixHQUFHLE1BQU0sQ0FBQyxDQUFDO0dBQzVEOztBQUVELE1BQUksV0FBVyxHQUFHLFNBQWQsV0FBVztXQUFTLElBQUksT0FBTyxDQUFDLFVBQUMsT0FBTyxFQUFFLE1BQU0sRUFBSztBQUN2RCxVQUFJLEdBQUcsR0FBRyxJQUFJLGNBQWMsRUFBRTtVQUMxQixLQUFLLEdBQUcsa0JBQUssR0FBRyxDQUFDLGNBQWMsQ0FBQyxDQUFDOztBQUVyQyxTQUFHLENBQUMsSUFBSSxDQUFDLE1BQU0sRUFBRSxHQUFHLEVBQUUsSUFBSSxDQUFDLENBQUM7OztBQUc1QixVQUFJLEtBQUssRUFBRTtBQUFFLFdBQUcsQ0FBQyxnQkFBZ0IsQ0FBQyxlQUFlLEVBQUUsU0FBUyxHQUFHLEtBQUssQ0FBQyxDQUFDO09BQUU7Ozs7O0FBS3hFLFNBQUcsQ0FBQyxNQUFNLEdBQUcsWUFBTTtBQUNqQixZQUFHLEdBQUcsQ0FBQyxNQUFNLElBQUksR0FBRyxJQUFJLEdBQUcsQ0FBQyxNQUFNLEdBQUcsR0FBRyxFQUFDO0FBQ3ZDLGNBQUksR0FBRyxDQUFDLFFBQVEsQ0FBQyxNQUFNLEdBQUcsQ0FBQyxFQUFFO0FBQzNCLG1CQUFPLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxHQUFHLENBQUMsUUFBUSxDQUFDLENBQUMsQ0FBQztXQUNuQyxNQUFNO0FBQ0wsbUJBQU8sQ0FBQyxJQUFJLENBQUMsQ0FBQztXQUNmO1NBQ0YsTUFBTTtBQUNMLGdCQUFNLENBQUMsZ0JBQWdCLENBQUMsR0FBRyxDQUFDLENBQUMsQ0FBQztTQUMvQjtPQUFDLENBQUM7OztBQUdMLFNBQUcsQ0FBQyxPQUFPLEdBQUc7ZUFBTSxNQUFNLENBQUMsSUFBSSxLQUFLLENBQUMsZUFBZSxDQUFDLENBQUM7T0FBQSxDQUFDOzs7QUFHdkQsVUFBSSxBQUFDLFdBQVcsQ0FBQyxPQUFPLENBQUMsTUFBTSxDQUFDLElBQUksQ0FBQyxJQUFNLElBQUksQ0FBQyxJQUFJLFlBQVksTUFBTSxBQUFDLEVBQUU7QUFDdkUsV0FBRyxDQUFDLGdCQUFnQixDQUFDLGNBQWMsRUFBRSxrQkFBa0IsQ0FBQyxDQUFDO0FBQ3pELFdBQUcsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQztPQUNyQyxNQUFNO0FBQ0wsV0FBRyxDQUFDLElBQUksRUFBRSxDQUFDO09BQ1o7S0FDRixDQUFDO0dBQUEsQ0FBQzs7O0FBR0gsU0FBTyxXQUFXLEVBQUUsQ0FBQyxJQUFJLENBQUMsVUFBQSxRQUFRO1dBQUksUUFBUTtHQUFBLEVBQUUsVUFBQSxHQUFHO1dBQy9DLEFBQUMsR0FBRyxDQUFDLE1BQU0sSUFBSSxHQUFHLEdBQ2QsT0FBTyxDQUFDLE1BQU0sQ0FBQyxHQUFHLENBQUMsR0FDbkIsa0JBQUssZUFBZSxFQUFFLENBQUMsSUFBSSxDQUFDO2FBQU0sV0FBVyxFQUFFO0tBQUEsQ0FBQztHQUFBLENBQ3ZELENBQUM7Q0FDSDs7QUFFRCxPQUFPLENBQUMsR0FBRyxHQUFLLFVBQUMsR0FBRztNQUFFLE1BQU0seURBQUMsRUFBRTtTQUFLLE9BQU8sQ0FBQyxHQUFHLEVBQUUsT0FBTyxDQUFDLEdBQUcsRUFBSyxFQUFFLE1BQU0sRUFBTixNQUFNLEVBQUUsQ0FBQztDQUFBLENBQUM7QUFDN0UsT0FBTyxDQUFDLEdBQUcsR0FBSyxVQUFDLEdBQUc7TUFBRSxNQUFNLHlEQUFDLEVBQUU7U0FBSyxPQUFPLENBQUMsR0FBRyxFQUFFLE9BQU8sQ0FBQyxNQUFNLEVBQUUsRUFBRSxNQUFNLEVBQU4sTUFBTSxFQUFFLENBQUM7Q0FBQSxDQUFDO0FBQzdFLE9BQU8sQ0FBQyxJQUFJLEdBQUksVUFBQyxHQUFHO01BQUUsSUFBSSx5REFBQyxFQUFFO1NBQU8sT0FBTyxDQUFDLEdBQUcsRUFBRSxPQUFPLENBQUMsSUFBSSxFQUFJLEVBQUUsSUFBSSxFQUFKLElBQUksRUFBRSxDQUFDO0NBQUEsQ0FBQztBQUMzRSxPQUFPLENBQUMsS0FBSyxHQUFHLFVBQUMsR0FBRztNQUFFLElBQUkseURBQUMsRUFBRTtTQUFPLE9BQU8sQ0FBQyxHQUFHLEVBQUUsT0FBTyxDQUFDLEtBQUssRUFBRyxFQUFFLElBQUksRUFBSixJQUFJLEVBQUUsQ0FBQztDQUFBLENBQUM7QUFDM0UsT0FBTyxDQUFDLEdBQUcsR0FBSyxVQUFDLEdBQUc7TUFBRSxJQUFJLHlEQUFDLEVBQUU7U0FBTyxPQUFPLENBQUMsR0FBRyxFQUFFLE9BQU8sQ0FBQyxHQUFHLEVBQUssRUFBRSxJQUFJLEVBQUosSUFBSSxFQUFFLENBQUM7Q0FBQSxDQUFDOztBQUUzRSxTQUFTLGdCQUFnQixDQUFDLE1BQU0sRUFBRTtBQUNoQyxNQUFJLEdBQUcsR0FBRyxFQUFFLENBQUM7O0FBRWIsT0FBSSxJQUFJLElBQUksSUFBSSxNQUFNLEVBQUU7QUFDdEIsUUFBSSxNQUFNLENBQUMsY0FBYyxDQUFDLElBQUksQ0FBQyxFQUFFO0FBQy9CLFVBQUksR0FBRyxHQUFHLElBQUk7VUFDVixLQUFLLEdBQUcsTUFBTSxDQUFDLElBQUksQ0FBQyxDQUFDOzs7QUFHekIsVUFBSSxLQUFLLENBQUMsT0FBTyxDQUFDLEtBQUssQ0FBQyxFQUFFO0FBQ3hCLFdBQUcsR0FBRyxHQUFHLENBQUMsTUFBTSxDQUFDLGVBQWUsQ0FBQyxHQUFHLEVBQUUsS0FBSyxDQUFDLENBQUMsQ0FBQztPQUMvQyxNQUFNO0FBQ0wsV0FBRyxDQUFDLElBQUksQ0FBQyxrQkFBa0IsQ0FBQyxHQUFHLENBQUMsR0FBRyxHQUFHLEdBQUcsa0JBQWtCLENBQUMsS0FBSyxDQUFDLENBQUMsQ0FBQztPQUNyRTtLQUNGO0dBQ0Y7O0FBRUQsU0FBTyxBQUFDLEdBQUcsQ0FBQyxNQUFNLEdBQUcsQ0FBQyxHQUFLLEdBQUcsR0FBRyxHQUFHLENBQUMsSUFBSSxDQUFDLEdBQUcsQ0FBQyxHQUFJLEVBQUUsQ0FBQztDQUN0RDs7O0FBR0QsT0FBTyxDQUFDLGdCQUFnQixHQUFHLGdCQUFnQixDQUFDOztBQUU1QyxTQUFTLGVBQWUsQ0FBQyxHQUFHLEVBQUUsR0FBRyxFQUFFO0FBQ2pDLFNBQU8sR0FBRyxDQUFDLEdBQUcsQ0FBQyxVQUFBLEdBQUc7V0FDaEIsa0JBQWtCLENBQUMsR0FBRyxDQUFDLEdBQUcsS0FBSyxHQUFHLGtCQUFrQixDQUFDLEdBQUcsQ0FBQztHQUFBLENBQzFELENBQUM7Q0FDSDs7QUFFRCxTQUFTLGdCQUFnQixDQUFDLEdBQUcsRUFBRTtBQUM3QixNQUFJLEdBQUcsR0FBRyxJQUFJLEtBQUssQ0FBQyxHQUFHLENBQUMsVUFBVSxDQUFDLENBQUM7QUFDcEMsS0FBRyxDQUFDLE1BQU0sR0FBRyxHQUFHLENBQUMsTUFBTSxDQUFDO0FBQ3hCLEtBQUcsQ0FBQyxRQUFRLEdBQUcsSUFBSSxDQUFDLEtBQUssQ0FBQyxHQUFHLENBQUMsUUFBUSxDQUFDLENBQUM7QUFDeEMsU0FBTyxHQUFHLENBQUM7Q0FDWjs7cUJBRWMsT0FBTyIsImZpbGUiOiJnZW5lcmF0ZWQuanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlc0NvbnRlbnQiOlsiKGZ1bmN0aW9uIGUodCxuLHIpe2Z1bmN0aW9uIHMobyx1KXtpZighbltvXSl7aWYoIXRbb10pe3ZhciBhPXR5cGVvZiByZXF1aXJlPT1cImZ1bmN0aW9uXCImJnJlcXVpcmU7aWYoIXUmJmEpcmV0dXJuIGEobywhMCk7aWYoaSlyZXR1cm4gaShvLCEwKTt2YXIgZj1uZXcgRXJyb3IoXCJDYW5ub3QgZmluZCBtb2R1bGUgJ1wiK28rXCInXCIpO3Rocm93IGYuY29kZT1cIk1PRFVMRV9OT1RfRk9VTkRcIixmfXZhciBsPW5bb109e2V4cG9ydHM6e319O3Rbb11bMF0uY2FsbChsLmV4cG9ydHMsZnVuY3Rpb24oZSl7dmFyIG49dFtvXVsxXVtlXTtyZXR1cm4gcyhuP246ZSl9LGwsbC5leHBvcnRzLGUsdCxuLHIpfXJldHVybiBuW29dLmV4cG9ydHN9dmFyIGk9dHlwZW9mIHJlcXVpcmU9PVwiZnVuY3Rpb25cIiYmcmVxdWlyZTtmb3IodmFyIG89MDtvPHIubGVuZ3RoO28rKylzKHJbb10pO3JldHVybiBzfSkiLCJcInVzZSBzdHJpY3RcIjtcblxuaW1wb3J0IGNvcmUgZnJvbSAnLi9jb3JlJztcbmltcG9ydCByZXN0IGZyb20gJy4vcmVzdCc7XG5pbXBvcnQgYXV0aCBmcm9tICcuL2F1dGgnO1xuaW1wb3J0IHVzZXIgZnJvbSAnLi9PbmU0NVVzZXInO1xuaW1wb3J0IGZvcm0gZnJvbSAnLi9PbmU0NUZvcm0nO1xuaW1wb3J0IGluc3RpdHV0aW9uIGZyb20gJy4vT25lNDVJbnN0aXR1dGlvbic7XG5cbi8qKlxuICogV3JhcHBlciBjbGFzcyBmb3IgYWxsIG9mIG9uZTQ1IHNlcnZpY2VzIGFuZCBjb25maWd1cmF0aW9uXG4gKi9cbmV4cG9ydCBkZWZhdWx0IGNsYXNzIE9uZTQ1IHtcblxuICBjb25zdHJ1Y3RvcigpIHtcbiAgICB0aGlzLkNvcmUgPSBjb3JlO1xuICAgIHRoaXMuUmVzdCA9IHJlc3Q7XG4gICAgdGhpcy5BdXRoID0gYXV0aDtcbiAgICB0aGlzLlVzZXIgPSB1c2VyO1xuICAgIHRoaXMuRm9ybSA9IGZvcm07XG4gICAgdGhpcy5JbnN0aXR1dGlvbiA9IGluc3RpdHV0aW9uO1xuICB9XG5cbiAgLyoqXG4gICAqIEBwYXJhbSBzZXJ2ZXJVcmwgLSBiYXNlIHVybCBlbmQgcG9pbnQgZm9yIG1ha2luZyBhcGkgcmVxdWVzdHNcbiAgICogQHBhcmFtIGNsaWVudEtleSAtIGFwcGxpY2F0aW9uIGtleVxuICAgKiBAcGFyYW0gY29uZmlnIC0ga2V5IHZhbHVlIGFycmF5IG9mIHBhcmFtZXRlcnMgdGhhdCBjYW4gYmUgc2V0IGllLiBDVVJSRU5UX1VTRVIsIFJFRlJFU0hfVE9LRU4sIEFDQ0VTU19UT0tFTlxuICAgKi9cbiAgaW5pdGlhbGl6ZShzZXJ2ZXJVcmwgPSAnJywgY2xpZW50S2V5ID0gJycsIGNvbmZpZ3MgPSBudWxsKSB7XG4gICAgdGhpcy5Db3JlLnNldCgnU0VSVkVSX1VSTCcsIHNlcnZlclVybCk7XG4gICAgdGhpcy5Db3JlLnNldCgnQ0xJRU5UX0tFWScsIGNsaWVudEtleSk7XG5cbiAgICBpZiAoY29uZmlncyBpbnN0YW5jZW9mIEFycmF5KSB7XG4gICAgICBjb25maWdzLmZvcmVhY2goKHZhbHVlLCBrZXkpID0+IHRoaXMuQ29yZS5zZXQoa2V5LCB2YWx1ZSkpXG4gICAgfVxuICAgIFxuICAgIHJldHVybiB0aGlzLkluc3RpdHV0aW9uLmdldEN1cnJlbnRJbnN0aXR1dGlvbigpO1xuICB9XG4gIFxuXG59XG4iLCIndXNlIHN0cmljdCc7XG5cbmltcG9ydCBjb3JlIGZyb20gJy4vY29yZSc7XG5pbXBvcnQgcmVzdCBmcm9tICcuL3Jlc3QnO1xuXG5mdW5jdGlvbiBnZXRCeUlkKGZvcm1faWQpIHtcbiAgcmV0dXJuIHJlc3QuZ2V0KCcvYXBpL3YxL2Zvcm1zLycgKyBmb3JtX2lkKTtcbn1cblxuZnVuY3Rpb24gZ2V0UXVlc3Rpb25zKGZvcm1faWQpIHtcbiAgIHJldHVybiByZXN0LmdldCgnL2FwaS92MS9mb3Jtcy8nICsgZm9ybV9pZCArICcvcXVlc3Rpb25zJyk7XG59XG5cblxuZXhwb3J0IGRlZmF1bHQge1xuICBnZXRCeUlkLFxuICBnZXRRdWVzdGlvbnNcbn07XG4iLCIndXNlIHN0cmljdCc7XG5cbmltcG9ydCBjb3JlIGZyb20gJy4vY29yZSc7XG5pbXBvcnQgcmVzdCBmcm9tICcuL3Jlc3QnO1xuXG5mdW5jdGlvbiBnZXRDdXJyZW50SW5zdGl0dXRpb24oKSB7XG4gIHJldHVybiByZXN0LmdldCgnL3B1YmxpYy9hcGkvdjEvaW5zdGl0dXRpb24nKTtcbn1cblxuZnVuY3Rpb24gZ2V0UHJlZmVyZW5jZXMocGFyYW1zKSB7XG4gIHJldHVybiByZXN0LmdldCgnL2FwaS92MS9pbnN0aXR1dGlvbi9wcmVmZXJlbmNlcycsIHBhcmFtcyk7XG59XG5cblxuZXhwb3J0IGRlZmF1bHQge1xuICBnZXRDdXJyZW50SW5zdGl0dXRpb24sXG4gIGdldFByZWZlcmVuY2VzXG59O1xuIiwiJ3VzZSBzdHJpY3QnO1xuXG5pbXBvcnQgY29yZSBmcm9tICcuL2NvcmUnO1xuaW1wb3J0IHJlc3QgZnJvbSAnLi9yZXN0JztcblxuZnVuY3Rpb24gbG9naW4odXNlcm5hbWUsIHBhc3N3b3JkKSB7XG4gIHJldHVybiByZXN0LmdldCgnL3B1YmxpYy9hcGkvdjEvdG9rZW4vbG9naW4nLCB7XG4gICAgdXNlcm5hbWUsXG4gICAgcGFzc3dvcmQsXG4gICAgY2xpZW50X2tleTogY29yZS5nZXQoJ0NMSUVOVF9LRVknKVxuICB9KS50aGVuKHJlcyA9PiB7XG4gICAgY29yZS5zZXQoJ0FDQ0VTU19UT0tFTicsIHJlcy5hY2Nlc3NfdG9rZW4pO1xuICAgIGNvcmUuc2V0KCdSRUZSRVNIX1RPS0VOJywgcmVzLnJlZnJlc2hfdG9rZW4pO1xuICAgIGNvcmUuc2V0KCdDVVJSRU5UX1VTRVInLCByZXMudXNlcik7XG4gICAgcmV0dXJuIHJlcztcbiAgfSk7XG59XG5cbi8qKlxuICogVG9kbyA6IFNob3VsZCByZXR1cm4gcHJvbWlzZSBhbmQgaW52YWxpZGF0ZSB0b2tlbiB1c2luZyBpbnZhbGlkYXRlIGFwaVxuICovXG5mdW5jdGlvbiBsb2dvdXQoKSB7XG4gICAgY29yZS5zZXQoJ0FDQ0VTU19UT0tFTicsIG51bGwpO1xuICAgIGNvcmUuc2V0KCdSRUZSRVNIX1RPS0VOJywgbnVsbCk7XG4gICAgY29yZS5zZXQoJ0NVUlJFTlRfVVNFUicsIG51bGwpO1xufVxuXG5mdW5jdGlvbiBnZXRDdXJyZW50VXNlcigpIHtcbiAgcmV0dXJuIGNvcmUuZ2V0KCdDVVJSRU5UX1VTRVInKTtcbn1cblxuZnVuY3Rpb24gZ2V0VG9kb3MocGFyYW1zKSB7XG4gIHJldHVybiByZXN0LmdldCgnL2FwaS92MS91c2VyL3RvZG9zJywgcGFyYW1zKTtcbn1cblxuZnVuY3Rpb24gZ2V0RXZhbHVhdGlvbkJ5SWQoaWQpIHtcbiAgcmV0dXJuIHJlc3QuZ2V0KCcvYXBpL3YxL3VzZXIvZXZhbHVhdGlvbnMvJyArIGlkKTtcbn1cblxuZnVuY3Rpb24gc2F2ZUFuc3dlcihldmFsdWF0aW9uX2lkLCBxdWVzdGlvbl9pZCwgdmFsdWVzKSB7XG4gIGxldCB1cmwgPSAnL2FwaS92MS91c2VyL2V2YWx1YXRpb25zLycgKyBldmFsdWF0aW9uX2lkICsgJy9xdWVzdGlvbi8nICsgcXVlc3Rpb25faWQgKyAnL2Fuc3dlcic7XG4gIHJldHVybiByZXN0LnB1dCh1cmwsIHZhbHVlcyk7XG59XG5cbmZ1bmN0aW9uIHBhdGNoRXZhbHVhdGlvbihldmFsdWF0aW9uX2lkLCB2YWx1ZXMpIHtcbiAgbGV0IHVybCA9ICcvYXBpL3YxL3VzZXIvZXZhbHVhdGlvbnMvJyArIGV2YWx1YXRpb25faWQ7XG4gIHJldHVybiByZXN0LnBhdGNoKHVybCwgdmFsdWVzKTtcbn1cblxuZXhwb3J0IGRlZmF1bHQge1xuICBsb2dpbixcbiAgbG9nb3V0LFxuICBnZXRDdXJyZW50VXNlcixcbiAgZ2V0VG9kb3MsXG4gIGdldEV2YWx1YXRpb25CeUlkLFxuICBzYXZlQW5zd2VyLFxuICBwYXRjaEV2YWx1YXRpb25cbn07XG4iLCIndXNlIHN0cmljdCc7XG5cbmltcG9ydCBjb3JlIGZyb20gJy4vY29yZSc7XG5pbXBvcnQgcmVzdCBmcm9tICcuL3Jlc3QnO1xuaW1wb3J0IGNvbmZpZyBmcm9tICcuL2NvbmZpZyc7XG5cbi8vIHRyeSB0byBnZXQgdG9rZW4gdXNpbmcgcmVmcmVzaCB0b2tlbjsgaWYgZmFpbGVkLFxuLy8gZmFsbGJhY2sgYW5kIHRyeSB0byBnZXQgdG9rZW4gYnkgY2xpZW50IGNyZWRlbnRpYWxzXG5mdW5jdGlvbiByZWdlbmVyYXRlVG9rZW4oKSB7XG4gIHJldHVybiBnZW5lcmF0ZVRva2VuQnlSZWZyZXNoVG9rZW4oKS5jYXRjaChlcnIgPT5cbiAgICBnZW5lcmF0ZVRva2VuQnlDbGllbnRDcmVkZW50aWFscygpXG4gICk7XG59XG4gIFxuZnVuY3Rpb24gZ2VuZXJhdGVUb2tlbkJ5UmVmcmVzaFRva2VuKCkge1xuICBsZXQgcmVmcmVzaFRva2VuID0gY29yZS5nZXQoJ1JFRlJFU0hfVE9LRU4nKTtcblxuICAvLyBpbW1lZGlhdGVseSByZWplY3Qgd2l0aG91dCBhdHRlbXB0aW5nIHJlcXVlc3QgaWYgcmVmcmVzaCB0b2tlblxuICAvLyBpcyBub3Qgc2V0XG4gIGlmICghcmVmcmVzaFRva2VuKSB7XG4gICAgcmV0dXJuIFByb21pc2UucmVqZWN0KG5ldyBFcnJvcignUmVmcmVzaCB0b2tlbiBpcyBub3Qgc2V0JykpO1xuICB9XG5cbiAgcmV0dXJuIHJlc3QuZ2V0KCcvcHVibGljL2FwaS92MS90b2tlbi9yZWZyZXNoJywge1xuICAgIHJlZnJlc2hfdG9rZW46IHJlZnJlc2hUb2tlblxuICB9KS50aGVuKHJlcyA9PiB7XG4gICAgY29yZS5zZXQoJ0FDQ0VTU19UT0tFTicsIHJlcy5hY2Nlc3NfdG9rZW4pO1xuICAgIGNvcmUuc2V0KCdSRUZSRVNIX1RPS0VOJywgcmVzLnJlZnJlc2hfdG9rZW4pO1xuICAgIHJldHVybiByZXM7XG4gIH0pO1xufVxuICBcbmZ1bmN0aW9uIGdlbmVyYXRlVG9rZW5CeUNsaWVudENyZWRlbnRpYWxzKCkge1xuICBsZXQgY2xpZW50S2V5ID0gY29yZS5nZXQoJ0NMSUVOVF9LRVknKVxuICAgICwgY2xpZW50U2VjcmV0ID0gY29yZS5nZXQoJ0NMSUVOVF9TRUNSRVQnKTtcblxuICAvLyBpbW1lZGlhdGVseSByZWplY3Qgd2l0aG91dCBhdHRlbXB0aW5nIHJlcXVlc3QgaWYgY2xpZW50IGtleSBvciBzZWNyZXRcbiAgLy8gaXMgbm90IHNldFxuICBpZiAoIWNsaWVudEtleSB8fCAhY2xpZW50U2VjcmV0KSB7XG4gICAgcmV0dXJuIFByb21pc2UucmVqZWN0KG5ldyBFcnJvcignQ2xpZW50IGtleSBvciBzZWNyZXQgaXMgbm90IHNldCcpKTtcbiAgfVxuXG4gIHJldHVybiByZXN0LmdldCgnL3B1YmxpYy9hcGkvdjEvdG9rZW4vZ2VuZXJhdGUnLCB7XG4gICAgY2xpZW50X2tleTogY2xpZW50S2V5LFxuICAgIGNsaWVudF9zZWNyZXQ6IGNsaWVudFNlY3JldFxuICB9KS50aGVuKHJlcyA9PiB7XG4gICAgY29yZS5zZXQoJ0FDQ0VTU19UT0tFTicsIHJlcy5hY2Nlc3NfdG9rZW4pO1xuICAgIGNvcmUuc2V0KCdSRUZSRVNIX1RPS0VOJywgcmVzLnJlZnJlc2hfdG9rZW4pO1xuICAgIHJldHVybiByZXM7XG4gIH0pO1xufVxuXG5leHBvcnQgZGVmYXVsdCB7XG4gIHJlZ2VuZXJhdGVUb2tlbixcbiAgZ2VuZXJhdGVUb2tlbkJ5UmVmcmVzaFRva2VuLFxuICBnZW5lcmF0ZVRva2VuQnlDbGllbnRDcmVkZW50aWFsc1xufTtcbiIsIid1c2Ugc3RyaWN0JztcblxuZXhwb3J0IGRlZmF1bHQge1xuICB0b2tlblBhdGg6ICcvcHVibGljL2FwaS92MS90b2tlbi9yZWZyZXNoJ1xufVxuIiwiJ3VzZSBzdHJpY3QnO1xuXG4vKipcbiAqIENvcmUgY2xhc3MgdG8gbWFuYWdlIHN0YXRlIHN1Y2ggYXMgYXV0aCBjcmVkZW50aWFscyBhbmQgY3VycmVudCB1c2VyXG4gKi9cbmNsYXNzIENvcmVNYW5hZ2VyIHtcblxuICBjb25zdHJ1Y3RvcigpIHtcbiAgICBsZXQga2V5cyA9IFtcbiAgICAgICdTRVJWRVJfVVJMJywgJ0NMSUVOVF9LRVknLCAnQ0xJRU5UX1NFQ1JFVCcsXG4gICAgICAnQUNDRVNTX1RPS0VOJywgJ1JFRlJFU0hfVE9LRU4nLCAnQ1VSUkVOVF9VU0VSJ1xuICAgIF07XG5cbiAgICB0aGlzLmtleXMgPSBrZXlzO1xuICAgIGtleXMuZm9yRWFjaChrZXkgPT4gdGhpc1trZXldID0gbnVsbCk7XG4gIH1cblxuICBnZXQoa2V5KSB7XG4gICAgcmV0dXJuIHRoaXMuaXNWYWxpZChrZXkpID8gdGhpc1trZXldIDogbnVsbDtcbiAgfVxuXG4gIHNldChrZXksIHZhbHVlKSB7XG4gICAgdGhpcy5pc1ZhbGlkKGtleSkgJiYgKHRoaXNba2V5XSA9IHZhbHVlKTtcbiAgfVxuXG4gIGlzVmFsaWQoa2V5KSB7XG4gICAgcmV0dXJuIHRoaXMua2V5cy5pbmRleE9mKGtleSkgPj0gMDtcbiAgfVxuXG59XG5cbmxldCBjb3JlO1xuXG5leHBvcnQgZGVmYXVsdCAoZnVuY3Rpb24gZ2V0Q29yZSgpIHtcbiAgY29yZSA9IGNvcmUgfHwgbmV3IENvcmVNYW5hZ2VyKCk7XG4gIHJldHVybiBjb3JlO1xufSkoKTtcbiIsIlwidXNlIHN0cmljdFwiO1xuXG5pbXBvcnQgT25lNDUgZnJvbSAnLi9PbmU0NSc7XG5leHBvcnQgZGVmYXVsdCBPbmU0NTtcblxud2luZG93ICYmICh3aW5kb3cuT25lNDUgPSBPbmU0NSk7XG4iLCIndXNlIHN0cmljdCc7XG5cbmltcG9ydCBjb3JlIGZyb20gJy4vY29yZSc7XG5pbXBvcnQgYXV0aCBmcm9tICcuL2F1dGgnO1xuXG5jb25zdCBtZXRob2RzID0ge1xuICBHRVQ6ICdHRVQnLCBQT1NUOiAnUE9TVCcsIFBVVDogJ1BVVCcsIFBBVENIOiAnUEFUQ0gnLCBERUxFVEU6ICdERUxFVEUnLFxuICBPUFRJT05TOiAnT1BUSU9OUycsIExJTks6ICdMSU5LJywgSEVBRDogJ0hFQUQnXG59O1xuXG5jb25zdCB3aXRoUGF5bG9hZCA9IFsgbWV0aG9kcy5QT1NULCBtZXRob2RzLlBVVCwgbWV0aG9kcy5QQVRDSCBdO1xuXG4vLyBUT0RPOiBhZGQgY3VzdG9tIGhlYWRlcnMgc3VwcG9ydFxuZnVuY3Rpb24gcmVxdWVzdCh1cmwsIG1ldGhvZCwgb3B0cz17fSkge1xuICAvLyBhZGQgcXVlcnkgcGFyYW1zIHRvIFVSTCBpZiB0aGVyZSBhcmUgYW55XG4gIHVybCA9IGNvcmUuZ2V0KCdTRVJWRVJfVVJMJykgKyB1cmxcbiAgICArICgob3B0cy5wYXJhbXMgaW5zdGFuY2VvZiBPYmplY3QpID8gX3NlcmlhbGl6ZVBhcmFtcyhvcHRzLnBhcmFtcykgOiAnJyk7XG5cbiAgbWV0aG9kID0gbWV0aG9kLnRvVXBwZXJDYXNlKCk7XG4gIGlmICghbWV0aG9kc1ttZXRob2RdKSB7XG4gICAgcmV0dXJuIGNvbnNvbGUuZXJyb3IoJ1Vuc3VwcG9ydGVkIGh0dHAgbWV0aG9kOiAnICsgbWV0aG9kKTtcbiAgfVxuXG4gIGxldCBtYWtlUHJvbWlzZSA9ICgpID0+IG5ldyBQcm9taXNlKChyZXNvbHZlLCByZWplY3QpID0+IHtcbiAgICBsZXQgcmVxID0gbmV3IFhNTEh0dHBSZXF1ZXN0KClcbiAgICAgICwgdG9rZW4gPSBjb3JlLmdldCgnQUNDRVNTX1RPS0VOJyk7XG5cbiAgICByZXEub3BlbihtZXRob2QsIHVybCwgdHJ1ZSk7XG5cbiAgICAvLyBhZGQgYWNjZXNzIHRva2VuIHRvIHBhcmFtcyBpZiBvbmUgaXMgc2V0XG4gICAgaWYgKHRva2VuKSB7IHJlcS5zZXRSZXF1ZXN0SGVhZGVyKCdBdXRob3JpemF0aW9uJywgJ0JlYXJlciAnICsgdG9rZW4pOyB9XG5cbiAgICAvLyByZXNvbHZlIHdpdGggcmVzcG9uc2UgaWYgc3RhdHVzIGNvZGUgaW5kaWNhdGVzIHN1Y2Nlc3MsXG4gICAgLy8gb3RoZXJ3aXNlIHJlamVjdCB3aXRoIGVycm9yXG4gICAgLy8gVE9ETzogaGFuZGxlIG5vbi1KU09OIHJlc3BvbnNlIGJvZGllc1xuICAgIHJlcS5vbmxvYWQgPSAoKSA9PiB7XG4gICAgICBpZihyZXEuc3RhdHVzID49IDIwMCAmJiByZXEuc3RhdHVzIDwgMzAwKXtcbiAgICAgICAgaWYgKHJlcS5yZXNwb25zZS5sZW5ndGggPiAwKSB7XG4gICAgICAgICAgcmVzb2x2ZShKU09OLnBhcnNlKHJlcS5yZXNwb25zZSkpO1xuICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgIHJlc29sdmUobnVsbCk7XG4gICAgICAgIH1cbiAgICAgIH0gZWxzZSB7XG4gICAgICAgIHJlamVjdChfY3JlYXRlSHR0cEVycm9yKHJlcSkpO1xuICAgICAgfX07XG5cbiAgICAvLyByZWplY3Qgd2l0aCBlcnJvciBvbiBuZXR3b3JrIGVycm9yc1xuICAgIHJlcS5vbmVycm9yID0gKCkgPT4gcmVqZWN0KG5ldyBFcnJvcignTmV0d29yayBFcnJvcicpKTtcblxuICAgIC8vIHNlbmQgdGhlIHJlcXVlc3QsIGluY2x1ZGluZyBkYXRhIGlmIG1ldGhvZCBpcyBQVVQsIFBBVENILCBvciBQT1NUXG4gICAgaWYgKCh3aXRoUGF5bG9hZC5pbmRleE9mKG1ldGhvZCkgPj0gMCkgJiYgKG9wdHMuZGF0YSBpbnN0YW5jZW9mIE9iamVjdCkpIHtcbiAgICAgIHJlcS5zZXRSZXF1ZXN0SGVhZGVyKFwiQ29udGVudC1UeXBlXCIsIFwiYXBwbGljYXRpb24vanNvblwiKTtcbiAgICAgIHJlcS5zZW5kKEpTT04uc3RyaW5naWZ5KG9wdHMuZGF0YSkpO1xuICAgIH0gZWxzZSB7XG4gICAgICByZXEuc2VuZCgpO1xuICAgIH1cbiAgfSk7XG5cbiAgLy8gY2F0Y2ggYW55IDQwMSBlcnJvcnMsIHRyeSB0byByZWdlbmVyYXRlIHRva2VuIGFuZCByZXRyeSAob25jZSBtYXgpXG4gIHJldHVybiBtYWtlUHJvbWlzZSgpLnRoZW4ocmVzcG9uc2UgPT4gcmVzcG9uc2UsIGVyciA9PlxuICAgICAgKGVyci5zdGF0dXMgIT0gNDAxKVxuICAgICAgICA/IFByb21pc2UucmVqZWN0KGVycilcbiAgICAgICAgOiBhdXRoLnJlZ2VuZXJhdGVUb2tlbigpLnRoZW4oKCkgPT4gbWFrZVByb21pc2UoKSlcbiAgKTtcbn1cblxucmVxdWVzdC5nZXQgICA9ICh1cmwsIHBhcmFtcz17fSkgPT4gcmVxdWVzdCh1cmwsIG1ldGhvZHMuR0VULCAgICB7IHBhcmFtcyB9KTtcbnJlcXVlc3QuZGVsICAgPSAodXJsLCBwYXJhbXM9e30pID0+IHJlcXVlc3QodXJsLCBtZXRob2RzLkRFTEVURSwgeyBwYXJhbXMgfSk7XG5yZXF1ZXN0LnBvc3QgID0gKHVybCwgZGF0YT17fSkgICA9PiByZXF1ZXN0KHVybCwgbWV0aG9kcy5QT1NULCAgIHsgZGF0YSB9KTtcbnJlcXVlc3QucGF0Y2ggPSAodXJsLCBkYXRhPXt9KSAgID0+IHJlcXVlc3QodXJsLCBtZXRob2RzLlBBVENILCAgeyBkYXRhIH0pO1xucmVxdWVzdC5wdXQgICA9ICh1cmwsIGRhdGE9e30pICAgPT4gcmVxdWVzdCh1cmwsIG1ldGhvZHMuUFVULCAgICB7IGRhdGEgfSk7XG5cbmZ1bmN0aW9uIF9zZXJpYWxpemVQYXJhbXMocGFyYW1zKSB7XG4gIGxldCBzdHIgPSBbXTtcblxuICBmb3IobGV0IHByb3AgaW4gcGFyYW1zKSB7XG4gICAgaWYgKHBhcmFtcy5oYXNPd25Qcm9wZXJ0eShwcm9wKSkge1xuICAgICAgbGV0IGtleSA9IHByb3BcbiAgICAgICAgLCB2YWx1ZSA9IHBhcmFtc1twcm9wXTtcbiAgICAgIC8vIFRPRE86IHNraXAgZW1wdHkgdmFscyBhbmQgdHJpbSBzdHJpbmdzIGhlcmVcblxuICAgICAgaWYgKEFycmF5LmlzQXJyYXkodmFsdWUpKSB7XG4gICAgICAgIHN0ciA9IHN0ci5jb25jYXQoX3NlcmlhbGl6ZUFycmF5KGtleSwgdmFsdWUpKTtcbiAgICAgIH0gZWxzZSB7XG4gICAgICAgIHN0ci5wdXNoKGVuY29kZVVSSUNvbXBvbmVudChrZXkpICsgXCI9XCIgKyBlbmNvZGVVUklDb21wb25lbnQodmFsdWUpKTtcbiAgICAgIH1cbiAgICB9XG4gIH1cblxuICByZXR1cm4gKHN0ci5sZW5ndGggPiAwKSA/ICgnPycgKyBzdHIuam9pbihcIiZcIikpIDogJyc7XG59XG5cbi8vIGV4cG9ydCBzZXJpYWxpemF0aW9uIGZuIGZvciB0ZXN0aW5nXG5yZXF1ZXN0Ll9zZXJpYWxpemVQYXJhbXMgPSBfc2VyaWFsaXplUGFyYW1zO1xuXG5mdW5jdGlvbiBfc2VyaWFsaXplQXJyYXkoa2V5LCBhcnIpIHtcbiAgcmV0dXJuIGFyci5tYXAodmFsID0+XG4gICAgZW5jb2RlVVJJQ29tcG9uZW50KGtleSkgKyAnW109JyArIGVuY29kZVVSSUNvbXBvbmVudCh2YWwpXG4gICk7XG59XG5cbmZ1bmN0aW9uIF9jcmVhdGVIdHRwRXJyb3IocmVxKSB7XG4gIGxldCBlcnIgPSBuZXcgRXJyb3IocmVxLnN0YXR1c1RleHQpO1xuICBlcnIuc3RhdHVzID0gcmVxLnN0YXR1cztcbiAgZXJyLnJlc3BvbnNlID0gSlNPTi5wYXJzZShyZXEucmVzcG9uc2UpO1xuICByZXR1cm4gZXJyO1xufVxuXG5leHBvcnQgZGVmYXVsdCByZXF1ZXN0O1xuIl19
